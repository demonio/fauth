package openfun.eu.fauth;

import net.canarymod.hook.HookHandler;
import net.canarymod.hook.player.ConnectionHook;
import net.canarymod.plugin.PluginListener;

public class MainListener implements PluginListener {
    @HookHandler
    public void onLogin(ConnectionHook hook) {
        hook.getPlayer().message("For login write /login PASSWORD");
        hook.getPlayer().setInvisible(true);
        hook.getPlayer().setCanBuild(false);
        hook.getPlayer().setMuted(true);
        /*hook.getPlayer().teleportTo(220, 64, 256);*/
    }
}
