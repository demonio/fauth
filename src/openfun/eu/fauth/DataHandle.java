package openfun.eu.fauth;

import net.canarymod.database.Column;
import net.canarymod.database.Column.DataType;
import net.canarymod.database.DataAccess;

public class DataHandle extends DataAccess {

    public DataHandle() {
        super("player_last_logout");
    }

    @Column(columnName = "password", dataType = DataType.STRING)
    public String player_password;

    @Column(columnName = "name", dataType = DataType.STRING)
    public String player_name;

    @Override
    public DataAccess getInstance() {
        return new DataHandle();
    }
}