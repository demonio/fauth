package openfun.eu.fauth;

import net.canarymod.Canary;
import net.canarymod.commandsys.CommandDependencyException;
import net.canarymod.commandsys.CommandListener;
import net.canarymod.commandsys.CommandOwner;
import net.canarymod.plugin.Plugin;
import net.canarymod.plugin.PluginListener;

@SuppressWarnings("unused")
public class Main extends Plugin { 

    private static final CommandOwner FAuth = null;

	@Override
    public boolean enable() {
        Canary.hooks().registerListener((PluginListener) new MainListener(), this);
        
        try {
			Canary.commands().registerCommands(new MainCommand(), FAuth, false);
		} catch (CommandDependencyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        getLogman().info("Enabling "+getName() + " Version " + getVersion()); //getName() returns the class name, in this case HelloWorld
        getLogman().info("Authored by "+getAuthor());
        return true;
    }

	@Override 
    public void disable() {   
        // TODO Auto-generated method stub 
    } 
}
