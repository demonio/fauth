package openfun.eu.fauth;

import net.canarymod.chat.MessageReceiver;
import net.canarymod.commandsys.Command;
import net.canarymod.commandsys.CommandListener;

public class MainCommand implements CommandListener {
    @Command(aliases = { "login" },
            description = "Login to game",
            permissions = { "openfun.command.login", "openfun.command.login" },
            toolTip = "/login password",
            min = 2)
    public boolean loginCommand(MessageReceiver caller, String[] parameters) {
        return new loginCommand().execute(caller, parameters);
    }
    @Command(aliases = { "register" },
            description = "Register to game",
            permissions = { "openfun.command.register", "openfun.command.register" },
            toolTip = "/register password",
            min = 2)
    public void registerCommand(MessageReceiver caller, String[] parameters) {
        new registerCommand().execute(caller, parameters);
    }
}
